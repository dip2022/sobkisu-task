<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(HomeController::class)->group(function(){
    Route::get('/','HomePage')->name('home.page');
});

Route::controller(AuthController::class)->group(function(){
    Route::get('/login','LoginPage')->name('login.page');
    Route::get('/register','RegisterPage')->name('register.page');
});
// Route::controller(AuthController::class)->group(function(){
    Route::post('/login-user', [AuthController::class, 'UserLogin'])->name('user.login');
    Route::post('/register-user',  [AuthController::class, 'UserRegister'])->name('user.register');
    Route::get('/logout-user',  [AuthController::class, 'LogoutUser'])->name('logout.user');
    Route::get('/password-forget', [AuthController::class, 'ForgetPassword'])->name('forget.password');
    Route::get('/otp-page', [AuthController::class, 'OtpMatchPage'])->name('otp.page');
    Route::post('/otp-match', [AuthController::class, 'OtpMatch'])->name('otp.match');
    Route::post('/reset-password', [MailController::class, 'index'])->name('reset.password');
    Route::get('/change-password/{email}', [AuthController::class, 'ChangePasswordPage'])->name('change.password.page');
    Route::post('/change-password', [AuthController::class, 'ChangePassword'])->name('change.password');
// });


Route::controller(ProductController::class)->group(function(){
    Route::get('/products', 'ProductViewPage')->name('product.view.page');
    Route::get('/product-add', 'ProductAddPage')->name('product.add.page');
    Route::get('/product-list', 'ProductListPage')->name('product.list.page');
    Route::get('/product-edit/{id}', 'ProductEditPage')->name('product.edit.page');
    Route::post('/product-add', 'ProductAdd')->name('product.add');
    Route::get('/product-addtocart/{id}', 'ProductAddToCart')->name('product.addtocart');
    Route::post('/product-edit/{id}', 'ProductEdit')->name('product.edit');
    Route::get('/product-delete/{id}', 'ProductDelete')->name('product.delete');
});

