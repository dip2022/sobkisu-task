<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCart;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    public function ProductViewPage(){
        $session_id = Session::getId();
        $products = Product::latest()->get();
        return view('products.product_view',compact('products'));
    }
    public function ProductAddPage(){
        return view('products.product_add');
    }
    public function ProductListPage(){
        $products = Product::latest()->get();
        return view('products.product_list',compact('products'));
    }
    public function ProductEditPage($id){
        $product = Product::findOrFail($id);
        return view('products.product_edit',compact('product'));
    }
    public function ProductAdd(Request $request){
        $this->validate($request, [
            'product_name' => 'required|string|max:255',
            'product_price' => 'required|string',
            'product_description' => 'required|string',
        ]);


        $product_add = Product::insert([
            'product_name'=>$request->product_name,
            'product_price'=>$request->product_price,
            'product_description'=>$request->product_description,
            'created_at'=>Carbon::now(),
        ]);

        if($product_add){   
            return redirect('/product-list')->with('success', 'Product inserted successfully!'); 
        }else {
            return redirect()->back()->with('error', 'Product not inserted!');   
        }
    }

    public function ProductAddToCart($id){
        $session_id = Session::getId();
        $data = Product::findOrFail($id);
        $check = ProductCart::where('session_id',$session_id)->where('product_id',$id)->count();
        if(!$check){
            $product_cart = ProductCart::insert([
                'session_id'=>$session_id,
                'product_id'=>$id,
                'product_name'=>$data->product_name,
                'product_price'=>$data->product_price,
                'product_description'=>$data->product_description,
                'created_at'=>Carbon::now(),
            ]);

            if($product_cart){   
                return redirect()->back()->with('success', 'Product Add To Cart successfully!'); 
            }else {
                return redirect()->back()->with('error', 'Product not Add To Cart!');   
            }
        }else{
            return redirect()->back()->with('error', 'Product Add To Cart Already!');   
        }
    }
    public function ProductEdit(Request $request,$id){
        $this->validate($request, [
            'product_name' => 'required|string|max:255',
            'product_price' => 'required|string',
            'product_description' => 'required|string',
        ]);


        $product_edit = Product::findOrFail($id)->update([
            'product_name'=>$request->product_name,
            'product_price'=>$request->product_price,
            'product_description'=>$request->product_description,
            'created_at'=>Carbon::now(),
        ]);

        if($product_edit){   
            return redirect('/product-list')->with('success', 'Product updated successfully!'); 
        }else {
            return redirect()->back()->with('error', 'Product not updated!');   
        }
    }
    public function ProductDelete($id){

        $product_delete = Product::findOrFail($id)->delete();

        if($product_delete){   
            return redirect('/product-list')->with('success', 'Product delete successfully!'); 
        }else {
            return redirect()->back()->with('error', 'Product not deleted!');   
        }
    }
}
